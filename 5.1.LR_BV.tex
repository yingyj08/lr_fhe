
\section{Leakage Resilient Fully Homomorphic Encryption with Evaluation Key}

\subsection{The \textsf{LRBV} Scheme}
	\label{sec:LRBV_modification}
	With the main theorem, we can show that (leveled) \textsf{BV} scheme can be leakage resilient although it has an evaluation key.
	In order to use the \textsf{Lossy} lemma and Theorem~\ref{thm:LR_LWE} to show the leakage resilience, we need to have a slightly different parameter setting. 
	More precisely, we need to use a larger (super-polynomial) noise distribution and binary secrets.
	
	The \textsf{LRBV} scheme is the same as \textsf{BV} scheme (see Section~\ref{sec:BV_scheme}) except:
	\begin{itemize}
		\item $\chi$ (noise distribution in $\ZZ_q^n$) is sampled from $\bar{\Psi}_\beta$, where $\beta = 2^{\log^2 \kappa}$. 
		\item The secret vectors $\textbf{s}_1,...,\textbf{s}_L$ in the evaluation key are all sampled uniformly from $\{0,1\}^n$ instead of $\ZZ_q^n$. \footnote{In our scheme, $\hat{\chi}$ is still $k$-bounded, and $\hat{\textbf{s}}$ is still sampled uniformly from $\ZZ_q^n$.}
		\footnote{With binary secret vectors, the evaluation key can be shorter. Here we keep the length of evaluation key unchanged, in order to keep our proof simple.}
	\end{itemize}
	
	As a reminder, we list all other unchanged parameters here. Denote the security parameter by $\kappa$,
	we choose the dimension $n = \textsf{poly}(\kappa)$, modulus $q \text{ is odd and }q \in [2^{n^\epsilon},2\cdot 2^{n^\epsilon}),\; \epsilon\in(0,1)$, 
	the upper bound of the maximal level of multiplication gates $L\approx \epsilon\log n$,
	the reduced dimension and modulus $k = \kappa, p = (n^2 \log q)\cdot \textsf{poly}(k)$, and the noise distribution $\hat{\chi}$ is a $k$-bounded distribution in $\ZZ_p^k$
	
	
	

\subsection{Leakage Resilience of the \textsf{LRBV} Scheme}
	We show that the LRBV scheme is semantically secure in the presence of bounded adaptive leakage.
	Formally, we have the following theorem.
	\begin{thm}
		\label{thm:BVFHE_LR}
		The leveled \textsf{LRBV} scheme is resilient to adaptive bounded leakage of $\lambda$ bits, where $\lambda\le\min\{n^{1-2\epsilon},k\}/4$.
	\end{thm}
	
	\begin{proof}
		It is equivalent to show that
		\begin{equation*}
			\begin{aligned}
				(\pk, \evk, \textsf{LRBV}.\Enc_\pk(0),h(\pk, \textbf{s}, \evk)) \approx_c  (\pk, \evk, \textsf{LRBV}.\Enc_\pk(1),h(\pk, \textbf{s},\evk)) \;,
			\end{aligned}
		\end{equation*}
		where $(\pk,\textbf{s},\evk)$ are defined by \eqref{eq:sk}, \eqref{eq:pk}, \eqref{eq:evk} with modifications as described in Section~\ref{sec:LRBV_modification}, 
		$\textsf{LRBV}.\Enc$ is the same as $\textsf{BV}.\Enc$ defined by \eqref{def:BVENC}.
	
	
	
	It is sufficient to show that
	\begin{equation*}
		( \pk, \evk, \overbrace{(\textbf{r}\textbf{A}_0, \textbf{r}\cdot(\textbf{A}_0\textbf{s}_0+2\textbf{e}_0) )}^{\textsf{LRBV}.\Enc_\pk(0)} ,\overbrace{h(\pk, \textbf{s}, \evk)}^{\Lambda} )
		\approx_{c}
		( \pk, \evk, \overbrace{(\textbf{u},u') }^{\text{uniform}} ,\overbrace{h(\pk, \textbf{s}, \evk)}^{\Lambda} ) \;,
	\end{equation*}
	where $\textbf{u}\in \ZZ_q^n$ and $u'\in\ZZ_q$ are uniformly random. For notation simplicity, we denote the leakage information $h(\pk, \textbf{s}, \evk)$ by $\Lambda$.
	We show a stronger statement where the adversary also have the joint distribution with $\textbf{r}\cdot\textbf{e}_0$. Namely,
	\begin{equation}
		\label{eq:hybrid_games}
		( \pk, \evk, ( \textbf{r} \textbf{A}_0 ,  \textbf{r}\textbf{A}_0\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda )
		\approx_{c}
		( \pk, \evk, (\textbf{u}, u', \textbf{r}\cdot\textbf{e}_0 ) ,\Lambda ) \;,
	\end{equation}
	where $\textbf{u}\overset{\$}\gets \ZZ_q^n$, $u'\overset{\$}\gets\ZZ_q$ and $\pk = (\textbf{A}_0,\textbf{A}_0\textbf{s}_0 + 2\textbf{e}_0)$.
	We show this by a series of hybrid games, $\mathcal{H}_0, \mathcal{H}_1, \mathcal{H}_2, \mathcal{H}_3, \mathcal{H}_4, \mathcal{H}_\textsf{rand}$, where $\mathcal{H}_0$ is the left side game in \eqref{eq:hybrid_games}, $\mathcal{H}_\textsf{rand}$ is the right side game in \eqref{eq:hybrid_games}. Our hybrids are:
	\begin{itemize}
		\item $\mathcal{H}_0 \overset{\bigtriangleup}{=} ( (\textbf{A}_0,\textbf{A}_0\textbf{s}_0 + 2\textbf{e}_0), \evk, ( \textbf{r} \textbf{A}_0 ,  \textbf{r}\textbf{A}_0\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda )$.
		\item $\mathcal{H}_1 \overset{\bigtriangleup}{=} ( (\textbf{A}_0,\textbf{A}_0\textbf{s}_0 + 2\textbf{e}_0), \evk, ( \textbf{u} ,  \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda )$, where $\textbf{u} \overset{\$}\gets \ZZ_q^N$.
		\item $\mathcal{H}_2 \overset{\bigtriangleup}{=} ( (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk, ( \textbf{u} ,  \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda' )$.
		\item $\mathcal{H}_3 \overset{\bigtriangleup}{=} ( (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', ( \textbf{u} ,  \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda'' )$.
		\item $\mathcal{H}_4 \overset{\bigtriangleup}{=} ( (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', ( \textbf{u} ,  u', \textbf{r}\cdot\textbf{e}_0 ),\Lambda'' )$, where $u' \overset{\$}\gets \ZZ_q$.
		\item $\mathcal{H}_\textsf{rand} \overset{\bigtriangleup}{=} ( (\textbf{A}_0,\textbf{A}_0\textbf{s}_0 + 2\textbf{e}_0), \evk, ( \textbf{u} ,  u', \textbf{r}\cdot\textbf{e}_0 ),\Lambda )$, where $u' \overset{\$}\gets \ZZ_q$.
	\end{itemize}
	
	\smallskip
	\begin{claim}
		$\mathcal{H}_0 \approx_s \mathcal{H}_1$.
	\end{claim}
	\begin{proof}
		It is sufficient to show $(\textbf{A}_0, \textbf{r}\textbf{A}_0, \textbf{r}\cdot\textbf{e}_0) \approx_s (\textbf{A}_0, \textbf{u}, \textbf{r}\cdot\textbf{e}_0)$, since everything else is independent of $\textbf{r}$.
		We have that
		\begin{equation*}
			\widetilde{\textsf{H}}_{\infty}(\textbf{r} |\;\textbf{r}\cdot\textbf{e}_0 ) 
			\ge \textsf{H}_{\infty}(\textbf{r}) - \log|\textbf{r}\cdot\textbf{e}_0| = m - \log{q} 
			\ge n\log{q} + 2\log{q}\;.
		\end{equation*}
		Use leftover hash lemma (use $\textbf{A}_0$ as the uniformly random seed and $\textbf{r}$ as the min entropy source), we get that the distributions $(\textbf{A}_0, \textbf{r}\textbf{A}_0, \textbf{r}\cdot\textbf{e}_0)$ and $(\textbf{A}_0, \textbf{u}, \textbf{r}\cdot\textbf{e}_0)$ are $(1/q)$-close. Since $q$ is super-polynomial to $n$, we have $1/q = \textsf{negl}(\kappa)$, which completes the proof.	
	\end{proof}
		
		
	
	\begin{claim}
		\label{claim:H1_H2}
		There exists a \textsf{Lossy} distribution defined in Lemma~\ref{lem:AGV}, such that for $\textbf{A}'_0\gets \textsf{Lossy}$ and the corresponding leakage information $\Lambda' = h(\pk',\bd{s},\evk)$, where $\pk' = (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0)$, giving rise to the hybrid
		$$\mathcal{H}_2 \overset{\bigtriangleup}{=} ( (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk, ( \textbf{u} ,  \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda' ) \;,$$
		such that
		\begin{enumerate}
			\item $\mathcal{H}_1 \approx_c \mathcal{H}_2$.
			\item $\widetilde{\textsf{H}}_{\infty}^{\smth}(\textbf{s}_0|\; (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0)) \ge n$,
		\end{enumerate}
	\end{claim}
	\begin{proof}
		The noise standard deviation $\beta$ is super-polynomial to the security parameter $\kappa$, and there exists a ``short'' dimension $n'$, such that $n'\le \frac{n-\log \kappa}{\log q} = \Theta( n^{1-\epsilon})$. Thus we can adopt Lemma~\ref{lem:AGV} to show the existence of the $\textsf{Lossy}$ distribution.	
		Moreover, notice that in $\mathcal{H}_1$, $\textbf{A}_0$ is independent of $\evk$ and $(\textbf{u}, \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0)$, the claim follows directly by Lemma \ref{lem:AGV}.
	\end{proof}
	
	
	\begin{claim}  %claim 3, crucial part
		\label{claim:H2_H3}
		There exist $\evk'$ and $\Lambda''$, giving rise to hybrid
		$$\mathcal{H}_3 \overset{\bigtriangleup}{=} ( (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', ( \textbf{u} ,  \textbf{u}\cdot\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda'' ) \;,$$
		such that
		\begin{enumerate}
		\item $\mathcal{H}_2 \approx_{c} \mathcal{H}_3$.
		\item $\widetilde{\textsf{H}}^{\smth}_{\infty}( \textsf{s}_{0} |\; (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', \Lambda'' ) \ge n - 2\lambda $.
		\end{enumerate}
	\end{claim}
	\begin{proof}
		This is the most important part in our proof since it deals with the evaluation key in the presence of leakage.
		Recall that the evaluation key is an instantiation of encryption chain:
		\begin{equation*}
			\begin{aligned}
				& \evk =	\\
				& ( (\textbf{A}_1, \textbf{A}_1\textbf{s}_1 + 2\textbf{e}_1 + \textbf{T}(\textbf{s}_0)), ..., (\textbf{A}_L, \textbf{A}_L\textbf{s}_L + 2\textbf{e}_L + \textbf{T}(\textbf{s}_{L-1})),   (\hat{\textbf{A}}, \hat{\textbf{A}}\hat{\textbf{s}} + \hat{\textbf{e}} + \hat{\textbf{T}}(\textbf{s}_L)) ) \;,
			\end{aligned}
		\end{equation*}
		where $\textbf{T}$ and $\hat{\textbf{T}}$ are some deterministic encoding functions. The $\textsf{DLWE}$ samples $(\textbf{A}_1, \textbf{A}_1\textbf{s}_1 + 2\textbf{e}_1),\ldots, (\textbf{A}_L, \textbf{A}_L\textbf{s}_L + 2\textbf{e}_L)$ have parameters $(q,n,\bar{\Psi}_\beta)$, denoted by $\textsf{DLWE}_{q,n,\beta}$. The last $\textsf{LWE}$ sample $ (\hat{\textbf{A}}, \hat{\textbf{A}}\hat{\textbf{s}} + \hat{\textbf{e}})$ is in the low dimension/modulus space with parameters $(p,k,\hat{\chi})$, denoted by $\textsf{DLWE}_{p,k,\hat{\chi}}$. In our setting, the $\textsf{DLWE}_{q,n,\beta}$ samples have binary secrets and the $\textsf{DLWE}_{p,k,\hat{\chi}}$ sample has the secret from the full domain, i.e., $\ZZ_p^k$.
		
		Consider the $\textsf{DLWE}_{p,k,\hat{\chi}}$, since $p$ is polynomial in $k$, the best known algorithms solve it in time $2^{\tilde{\Omega}(k)}$. Thus, it is $(\varepsilon,s_1)$-indistinguishable from uniform, where $s_1$ can take any value in $2^{\tilde{o}(k)}$ and $\varepsilon = \textsf{negl}(\kappa)$.
		
		For $\textsf{DLWE}_{q,n,\beta}$, consider the case that the secret is sampled non-uniformly over $\{0,1\}^n$ from distribution $\mathcal{D}$ with min-entropy at least $n-2\lambda$ (we use this value in order to meet the assumption in Theorem~\ref{thm:generalized_main}). By Theorem~\ref{thm:LR_LWE}, for any $\ell\le \frac{n-2\lambda-\omega(\log{n})}{\log q}$, there is a P.P.T.\ reduction from $\textsf{DLWE}_{\ell,q,\alpha}$ to $\textsf{DLWE}_{n,q,\beta}(\mathcal{D})$. In our setting, we can choose $\ell=\tilde{O}(n^{1-\epsilon})$, such that the best known algorithms solve $\textsf{DLWE}_{\ell,q,\alpha}$ in time $2^{\tilde{\Omega}(n^{1-2\epsilon})}$. Thus, $\textsf{DLWE}_{n,q,\beta}(\mathcal{D})$ is $(\varepsilon,s_2)$-indistinguishable from uniform, where $s_2$ can take any value in $2^{\tilde{o}(n^{1-2\epsilon})}$ and $\varepsilon = \textsf{negl}(\kappa)$.
		
		In summary, we have that $\textsf{DLWE}_{p,k,\hat{\chi}}$ and $\textsf{DLWE}_{q,n,\beta}$ are $(\varepsilon,s)$-indistinguishable from uniform, where $s$ can take any value in $2^{\tilde{o}(\min\{n^{1-2\epsilon},k\})}$, $\varepsilon = \textsf{negl}(\kappa)$. By Theorem \ref{thm:generalized_main}, there exists $\mathcal{H}_3$ by replacing $(\evk,\Lambda)$ to $(\evk',\Lambda'')$, such that $\mathcal{H}_2 \approx_{(2L\varepsilon,\hat{s})} \mathcal{H}_3$, and 
		\begin{equation*}
			\begin{aligned}
				&\widetilde{\textsf{H}}_{\infty}^{\smth}( \textsf{s}_{0} \;|\; (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', \Lambda'' )\\
				 \ge\; & \widetilde{\textsf{H}}_{\infty}^{\smth}( \textsf{s}_{0} \;|\; (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0)) - 2\lambda	\\
				 \overset{\text{(a)}}{\ge}\; & n - 2\lambda	\;,
			\end{aligned}
		\end{equation*}
		where inequality (a) follows by Claim \ref{claim:H1_H2}, and $\hat{s} = \Theta(s\cdot \frac{\varepsilon^2}{2^{3\lambda}\log^2 (1/\varepsilon)})$. The quality of the indistinguishability degrades exponentially in $\lambda$. By choosing $\lambda = \min\{n^{1-2\epsilon},k\}/4$, we can keep the computational indistinguishability between $\mathcal{H}_2$ and $\mathcal{H}_3$.
	\end{proof}
	
	
	\begin{claim}
		$\mathcal{H}_3 \approx_s \mathcal{H}_{4}$.
	\end{claim}
	\begin{proof}
		We have
		\begin{equation*}
			\begin{aligned}
				 & \widetilde{\textsf{H}}_{\infty}^{\smth}( \textsf{s}_{0} \;|\; \mathcal{H}_3\backslash\{\textbf{u} ,  \textbf{u}\cdot\textbf{s}_0\} ) \\
				\overset{\text{(a)}}{\ge} \;& \widetilde{\textsf{H}}_{\infty}^{\smth}( \textsf{s}_{0} \;|\; (\textbf{A}'_0,\textbf{A}'_0\textbf{s}_0 + 2\textbf{e}_0), \evk', \Lambda'' ) - |\textbf{r}\cdot\textbf{e}_0|	\\
				 \overset{\text{(b)}}{\ge} \;& n - 2\lambda - \log{q} \;.
			\end{aligned}
		\end{equation*}
		The inequality (a) holds by the chain rule of min-entropy.
		The inequality (b) holds by Claim~\ref{claim:H2_H3} and the fact that $\textbf{r}\cdot\textbf{e}_0 \in \ZZ_q$.
		Since in BV's FHE scheme, $q$ is sub-exponential of $n$, we get that $n - 2\lambda - \log{q} \ge |\textbf{u}\cdot\textbf{s}_0| + 2\log{q}$,
		which allows us to use leftover hash lemma again, to show $(\textbf{u}, \textbf{u}\cdot\textbf{s}_0)$ and $(\textbf{u}, u')$ are $(1/q)$-close even given all the other elements in the hybrid. Since $1/q = \textsf{negl}(\kappa)$, the claim holds.	
	\end{proof}
	
	\begin{claim}
		$\mathcal{H}_4 \approx_c \mathcal{H}_{\textsf{rand}}$
	\end{claim}
	\begin{proof}
		By switching $(\textsf{pk}', \textsf{evk}', \Lambda'')$ back to $(\textsf{pk}, \textsf{evk}, \Lambda)$, this claim holds.
	\end{proof}

	Putting these claims together, we have $\mathcal{H}_0\approx_s\mathcal{H}_1\approx_c\mathcal{H}_2 \approx_c \mathcal{H}_3 \approx_s \mathcal{H}_4 \approx_c \mathcal{H}_{\textsf{rand}}$. 
	Using the triangle inequality of computational indistinguishability\footnote{which states that for any random variable $\alpha,\beta,\gamma$ we have $$\alpha\approx_{(\varepsilon_1,s_1)}\beta \text{ and } \beta \approx_{(\varepsilon_2,s_2)} \gamma \to \alpha \approx_{(\varepsilon_1+\varepsilon_2,\min(s_1,s_2))} \gamma \;.$$}
	, we get $\mathcal{H}_0 \approx_c \mathcal{H}_{\textsf{rand}}$, which completes the proof.
	
	\begin{comment}
	More precisely, we get 
	\begin{equation*}
		( \pk, \evk, ( \textbf{r} \textbf{A}_0 ,  \textbf{r}\textbf{A}_0\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda )
		\approx_{(\varepsilon',s')}
		( \pk', \evk', (\textbf{u}, u', \textbf{r}\cdot\textbf{e}_0 ) ,\Lambda'' ) \;,
	\end{equation*}
	which implies
	\begin{equation*}
		( \pk, \evk, ( \textbf{r} \textbf{A}_0 ,  \textbf{r}\textbf{A}_0\textbf{s}_0, \textbf{r}\cdot\textbf{e}_0 ),\Lambda )
		\approx_{(\varepsilon',s')}
		( \pk, \evk, (\textbf{u}, u', \textbf{r}\cdot\textbf{e}_0 ) ,\Lambda'' ) \;.
	\end{equation*}
	\end{comment}
	
	
	
	\end{proof}

	

	