
\section{Introduction}
	Fully homomorphic encryption (FHE), is a cryptographic mechanism which allows us to homomorphically do arbitrary computations on the encrypted data without being able to decrypt it.
	To date, there have existed a lot of FHE schemes. However, all of these schemes have been given security proofs in the standard model, but not in the leakage model.     
	
	In this semester project, we are going to show that the Brakerski and Vaikuntanathan's LWE-based FHE scheme (BV's FHE scheme) \cite{BrakerskiV11} is not only semantically secure in the standard model, but also leakage resilient against bounded adaptive memory attacks with suitable parameters.
	Here, leakage resilience means that the scheme is still semantically secure if it leaks a small amount of information (usually a fraction of bits of information w.r.t.\ the length of the secret).

	\subsection{Leakage Models}
	In side channel attacks, adversary can get extra information via various ways, such as power consumption monitoring, electromagnetic analysis, timing analysis, etc.
	In the cryptographic leakage model, we use information theoretical notations to model the leakage information from any kind of side channel attacks to abstract the physical details away.
	
	Suppose we have an encryption scheme with $\kappa$-bits of security, and the adversary can get extra information of at most $\lambda$ bits, where $\lambda$ is some constant in $(0,\kappa)$. We hope that we can get a scheme with $(\kappa-\lambda)$-bits of security in the presence of leakage. However, in real cryptographic schemes, small amount of leakage may cause large security loss. There are known negative results and positive results.
	It has been shown that we can break RSA public-key scheme by factoring $N=pq$ given a small fraction of the bits of one of the factors \cite{Coppersmith96a,HeningerS09}.
	It has been proved recently in \cite{GoldwasserKPV10,AkaviaGV09} that LWE problem is resilient to small amount of information leakage and the Regev's public key scheme \cite{Regev05} is also leakage resilient.

	
	In BV's FHE scheme, besides the normal public/secret keys, there exists an evaluation key, which is used for homomorphic evaluations and is also public. In our leakage model, the adversary can choose a bounded (efficiently computable) leakage function $h(\pk,\bd{s},\evk)$\footnote{Here we use notation $\bd{s}$ to denote all the secret vectors in BV's FHE scheme, since besides the secret key \sk, we also have some other secret vectors to generate the evaluation key \evk, although these secret vectors will never be used in the evaluation or decryption. }
	as an oracle and have access to its output. Notice that the leakage function chosen by the adversary can also depend on the public components \pk and \evk, which is equivalent to let the adversary first see \pk and \evk, then choose the leakage function $h(\bd{s})$ according to what he has seen.
	This is called \emph{adaptive memory attack}, which will be formally defined later.
	We also have a weaker notion called \emph{non-adaptive memory attack}, in which the adversary can choose a bounded (efficiently computable) leakage function $h(\bd{s})$ which can only depend on $\bd{s}$. This means the adversary must choose one leakage function $h(\bd{s})$ before seeing \pk and \evk.
	In this project we will focus on the adaptive memory attack model, which is more natural since in side channel attacks, leakage information may depend on any component.
	
	In some public-key schemes like RSA and ElGamal, adaptive and non-adaptive memory attacks are equivalent because in these schemes the public key is a deterministic function of the secret key. We denote the relation by $\pk=f(\sk)$. For any leakage function $h(\pk,\sk)$ chosen by the adversary using adaptive memory attack, the adversary using non-adaptive memory attack can choose the corresponding function $h(f(\sk),\sk)$. However, in LWE-based public-key schemes like Regev's scheme, adaptive and non-adaptive memory attacks are not equivalent because the public key contains some randomness and is not a deterministic function of the secret key, so that the adversary using non-adaptive memory attack can not predict the public key using the secret key.
	
	We show that BV's FHE scheme is semantically secure against bounded adaptive memory attack, which can be defined as:
	\begin{equation*}
		\begin{aligned}
			& |\text{Pr}[\mathcal{A}(\pk, \evk,\textsf{BV}.\Enc_{\pk}(0),h(\pk, \textbf{s}, \evk))=1]|\\
			 -& |\text{Pr}[\mathcal{A}(\pk, \evk,\textsf{BV}.\Enc_{\pk}(1),h(\pk, \textbf{s},\evk))=1]|=\textsf{negl}(\kappa) \;,
		\end{aligned}
	\end{equation*}
	where the output length of $h(\pk, \textbf{s}, \evk)$ is short.
	
	\subsection{Related Work}
	The first FHE scheme was proposed by Gentry \cite{Gentry09} in 2009, using a bootstrappable scheme.
	To date, there have existed numerous FHE schemes following Gentry's blueprint, among which there are three main categories: FHE based on ideal lattices \cite{Gentry09,SmartV11,GentryH11,GentryHS12},
	FHE over the integers \cite{vanDijkGHV10,CoronMNT11,CoronNT12,CheonCKLLTY13},
	and FHE based on the learning with error problem \cite{BrakerskiV11,BrakerskiGV12,Brakerski12,BrakerskiGH13,GentrySW13}.	
	Our proof uses the LWE-based FHE scheme proposed by Brakerski and Vaikuntanathan in \cite{BrakerskiV11}.
	
	In \cite{BerkoffL}, Berkoff and Liu have showed that BV's FHE scheme is secure if the adversary can choose a bounded leakage function $h(\pk,\bd{s})$. Their leakage model is not a fully adaptive memory attack model since the leakage function cannot depend on the evaluation key (which is not predictable by $\pk$ and $\bd{s}$). They avoided the most troublesome part of the evaluation key.
	
	Recently in \cite{BerkoffL14}, Berkoff and Liu constructed the first bounded adaptive leakage resilient FHE scheme. Their scheme is based on the FHE scheme constructed by Gentry, Sahai, and Waters \cite{GentrySW13}, which is an FHE scheme without any evaluation key.
	In the paper of Berkoff and Liu, they claimed that: ``Since the presence of an evaluation key is what hampers the proof, our next step is to apply this technique to a scheme without an evaluation key.''
	
	In our work, the evaluation key obstacle has been overcome. With our technique, we can extend the leakage resilience concept to other FHE schemes with evaluation keys.
	
	
	
