
\section{The Leakage Resilience of the Encryption Chain}
	\label{sec:encrypt_chain}

	\subsection{The Main Theorem}
	Here we show a more general theorem which can be used to overcome the evaluation key obstacle for leakage resilience FHE.

	We first give the definition for a secure randomized wPRF with non-uniform weak key distributions.

\begin{definition}[Weak Randomized PRF \cite{ApplebaumCPS09}]
	An efficiently computable randomized function family $\textsf{F} : \mathcal{K} \times \mathcal{X} \to \mathcal{Y}$ is called a randomized weak pseudorandom function (RWPRF) if it satisfies the following:
\begin{itemize}
	\item (weak pseudorandomness) For every $n = \textsf{poly}(k)$, the sequence
	$$(X_1 , \textsf{F}_{K}(X_1), . . . , X_n , \textsf{F}_K(X_n))\; \text{is pseudorandom,}$$ where $K \gets U_k$ and $(X_1,\ldots,X_n ) \gets (U_{\mathcal{X}})^n$ and fresh internal randomness
is used in each evaluation of $\textsf{F}_K$ .
	\item (verifiability) There exists an efficient equality-tester algorithm $V$ such that
	\begin{equation*}
	\begin{aligned}	
	Pr[V(Y_1,Y_2)=\textsf{equal}] &> 1-\textsf{negl}(n) \\
	Pr[V(Y_1,Y_2 )=\textsf{not-equal}] &> 1-\textsf{negl}(n),
	\end{aligned}
	\end{equation*}
	where $K\gets U_k$, $X\gets U_{\mathcal{X}}$, $X\gets U_{\mathcal{X}}$, $Y_1\gets\textsf{F}_K(X)$, $Y_2\gets\textsf{F}_K(X)$, and $Y_2\gets\textsf{F}_K(X')$.
\end{itemize}
\end{definition}	

\begin{definition}
	A distribution $K$ over $\{0,1\}^k$ is $\ell$-weak, if its min-entropy is larger than or equal to $k-\ell$, namely,
	$$\textsf{H}_\infty (K) \ge k - \ell \;.$$
\end{definition}
	
\begin{definition}
A RWPRF $\textsf{F}$: $\mathcal{K} \times \mathcal{X} \to\mathcal{Y}$ is $(\varepsilon,s)$-indistinguishable from uniform for any $\ell$-weak key distribution, if for any $\ell$-weak key distribution $K$ over $\mathcal{K}$,\footnote{This notation is stronger (up to the right choice of the parameters) than a setting where the key is uniform distributed and the adversary can get at most $\ell$ bits of leakage information about the key. The input $X$ is uniformly sampled and the adversary can not choose the input. }
and an input $X$ sampled uniformly in $\mathcal{X}$ (independent of the key), it holds that
\begin{equation}
	\label{PRF_property}
	(X,\textsf{F}_{K}(X))\approx_{(\varepsilon,s)} (X,U_{\mathcal{Y}})	\;,
\end{equation}
where we denote the uniform distribution over $\mathcal{Y}$ by $U_{\mathcal{Y}}$.
\end{definition}
	Let $\textsf{F}$: $\mathcal{K} \times \mathcal{X} \to\mathcal{Y}$ be a $(\varepsilon,s)$-RWPRF for $\ell$ weak keys.
	%Now we are given a sequence of RWPRFs $Y_1 = \textsf{F}^{(1)}_{K_1}(X_1), \ldots, Y_n = \textsf{F}^{(n)}_{K_n}(X_n)$ that are indistinguishable from uniform,\footnote{Note that the functions $\textsf{F}^{(1)},\ldots,\textsf{F}^{(n)}$ can be the same function, but they can also be different functions even with different key/input/output domains. } 
and $n$ (deterministic) functions $T^{(i)}(K_{i-1}), i=1,\ldots,n$ that map (encode) any value in the input domain $\mathcal{K}$ to the domain $\mathcal{Y}$.
	We define the following random variable that we sometimes also will call the encryption chain. More precisely, for keys $K_i \draw \cK$ and inputs $X_i \draw \cX$ we define:
	\begin{equation*}
		\text{Encryption Chain} \overset{\bigtriangleup}{=}
		\left(
		\begin{array}{c}
			(X_1,\textsf{F}_{K_1}(X_1)+ T^{(1)}(K_0)),\\
			(X_2,\textsf{F}_{K_2}(X_2)+ T^{(2)}(K_1)),\\
			\vdots\\
			(X_n,\textsf{F}_{K_n}(X_n)+ T^{(n)}(K_{n-1}))
		\end{array} \right)		
	\end{equation*}
	where we use $\textsf{F}^{(i)}_{K_i}(X_i)$ as an one-time pad, and $T^{(i)}(K_{i-1})$ is the plaintext of the one-time pad.
	In the encryption chain, $K_0$ is the secret information that we want to hide, and whose security depends on all the encryptions in the chain.
	If the transform functions $T^{(1)},\ldots,T^{(n)}$ are efficiently invertible, the encryption chain is decryptable, which means with the last key $K_n$ we can decrypt it to recover the values of $(K_0,\ldots,K_{n-1})$, otherwise the encryption chain is undecryptable. However, we do not care about whether the encryption chain is decryptable or not here. We only care about whether the encryption chain computationally hides $K_0$ in the presence of bounded leakage, which will later enable us to prove adaptive leakage resilience of the FHE scheme even conditioned on the evaluation key.

	
\begin{thm}[Main]
	\label{thm:main}
	Let $\textsf{F}$: $\{0,1\}^k\times \mathcal{X} \to\mathcal{Y}$ be a $(\varepsilon,s)$-family of RWPRFs for $2\ell$-weak keys.
	Let $T^{(i)}: \cK \rightarrow \cY$ for $i=1,\ldots,n$ be some deterministic mapping. Let $K_0, \ldots , K_n$ be drawn uniformly at random from $\cK$ and $X_1, \ldots , X_n$ be drawn uniformly at random from $\cX$. Let $W_0$ be a random variable that is independent from $X_i, K_j$ and $\Lambda$ be a random variable over $\bit^\ell$ (which may depend on all random variables). We define $Y_i := F_{K_i}(X_i)$ and:
		\begin{equation*}
			\begin{aligned}
				\textsf{view} & = \left(
				\begin{array}{c}
					W_0,	\\
					(X_1,Y_1+T^{(1)}(K_0)),\\
					(X_2,Y_2+T^{(2)}(K_1)),\\
					\vdots\\
					(X_n,Y_n+T^{(n)}(K_{n-1})),\\
					\Lambda
				\end{array}	
				\right)	,
			\end{aligned}
		\end{equation*}
		There exist random variables $Z_1,\ldots,Z_n, \Lambda'$, from which we can construct $\textsf{view}'$ such that
		\begin{equation*}
				\textsf{view}' = \left(
				\begin{array}{c}
					W_0,	\\
					(X_1,Z_1+ T^{(1)}(K_0)),\\
					(X_2,Z_2+ T^{(2)}(K_1)),\\
					\vdots\\
					(X_n,Z_n+ T^{(n)}(K_{n-1})),\\
					\Lambda'
				\end{array}	
				\right)	,
		\end{equation*}
		such that
		\begin{equation}
			\label{eq:main_thm_obj}
			\left\{
			\begin{aligned}
				&(K_0,\textsf{view}') \approx_{(2n\varepsilon,\hat{s})} (K_0,\textsf{view})	\\
				&\widetilde{\textsf{H}}_\infty(K_0|\;\textsf{view}' ) \ge \textsf{H}_\infty(K_0| W_0) - 2\ell
			\end{aligned}
			\right.	\;,
		\end{equation}
		where $\hat{s} = \Theta(s\cdot \frac{\varepsilon^2}{2^{3\ell}\log^2 (1/\varepsilon)})$.
	\end{thm}
	
	\begin{proof}
The basic idea of the proof is to define an adjusted view $\view'$ that is computationally close to the original view $\view$ but leaves high average min entropy in the initial secret key $K_0$. To this end, we will define several hybrid views $\view^{(i)}$, where we move from the end of the encryption chain to the beginning always replacing parts of the distribution with fake values. We will show that two consecutive views are close and that conditioned on the values in the view $\view^{(i)}$ all keys $K_{i-1}, \ldots ,K_n$ have high average min-entropy. We notice that $\view = \view^{(n+1)}$ and $\view' = \view^{(1)}$. More precisely, we define
		\begin{equation*}
				\textsf{view}^{(i)} = \left(
				\begin{array}{c}
					W_0,	\\
					(X_1,Y_1+ T^{(1)}(K_0)),\\
					(X_2,Y_2+ T^{(2)}(K_1)),\\
					\vdots\\
					(X_{i-1},Y_{i-1}+ T^{(i-1)}(K_{i-2})),\\
					(X_i,Z_i+ T^{(i)}(K_{i-1})),\\
					\vdots\\
					(X_n,Z_n+ T^{(n)}(K_{n-1})),\\
					\Lambda^{(i)}
				\end{array}	
				\right)	, \;\text{for}\; \ i=1,\ldots,n, n+1	\;,
		\end{equation*}
		where $\Lambda^{(n)} = \Lambda$ and $\Lambda^{(1)} = \Lambda'$. Notice that $\textsf{view}^{(i+1)}$ is as $\textsf{view}^{(i)}$ except that we replace $Y_i$ and $\Lambda^{(i+1)}$ by $Z_i$ and $\Lambda^{(i)}$.

For ease of notation, we denote all the replaced encryptions having the form $(X,Z+ T(K))$ in $\textsf{view}^{(i)}$ by $E_i$, and we denote the  joint distribution of random variables with indices less than or equal to $i$ by $R_i$. More precisely,
		\begin{align}
			\label{eq:def_Ei}
			E_i & = ((X_i,Z_i+ T^{(i)}(K_{i-1})) \ldots (X_n,Z_n+ T^{(n)}(K_{n-1}))) \;, \quad i = 1,\ldots,n \;,	\\
			\label{eq:def_Ri}
			R_i & = \left(K_0, W_0, (X_1,Y_1,K_1),\ldots, (X_{i},Y_{i},K_{i}) \right) \;, \quad i = 0,\ldots,n	\;.
		\end{align}

 We first notice that for any index $i\in\{2,\ldots,n\}$, the tuple $(K_i,X_i)$ is independent of $R_{i-2}$. This clearly implies that the first $i-1$ encryptions $(X_1,Y_1+K_0),\ldots, (X_{i-1},Y_{i-1}+K_{i-2})$ do not reveal any information about $K_i$. Hence, we will usually ignore these values when we move backwards.
		
We use induction to construct the sequence of views one by one.
\begin{claim}[Induction]
	\label{claim:induction_for_main}
	For any $k\in\{2,\ldots,n+1\}$, let $K_0,\ldots ,K_n$ be uniformly and independently chosen from $\cK$. Let $X_1, \ldots , X_n$ be chosen uniformly at random from $\cX$. If there exists $Z_n, \ldots, Z_k$ and $\Lambda^{(k)}$ such that
		\begin{equation*}
			\textsf{view}^{(k)} = \left(
			\begin{array}{c}
				W_0,	\\
				(X_1,Y_1+ T^{(1)}(K_0))\\
				(X_2,Y_2+ T^{(2)}(K_1))\\
				\vdots\\
				(X_{k-1},Y_{k-1}+ T^{(k-1)}(K_{k-2}))\\
				(X_k,Z_k+ T^{(k)}(K_{k-1}))\\
				\vdots\\
				(X_n,Z_n+ T^{(n)}(K_{n-1}))\\
				\Lambda^{(k)}
			\end{array}	
			\right)	\;,
		\end{equation*}
		and
		\begin{equation*}
		\widetilde{\textsf{H}}_{\infty}( K_{k-1} | E_k, \Lambda^{(k)}) \ge |K_{k-1}| - 2\ell	\;,
		\end{equation*}
		where $E_i$ is defined as in \eqref{eq:def_Ei}.\footnote{For $k = n+1$, the induction hypothesis becomes $\widetilde{\textsf{H}}_{\infty}( K_n | \Lambda) \ge |K_n| - 2\ell$, which holds trivially.  Thus we have the induction base.}
		There exists $Z_{k-1}$ and $\Lambda^{(k-1)}$such that for
		\begin{equation}
			\label{eq:induction_new_view}
			\textsf{view}^{(k-1)} = \left(
			\begin{array}{c}
				W_0,	\\
				(X_1,Y_1+ T^{(1)}(K_0))\\
				(X_2,Y_2+ T^{(2)}(K_1))\\
				\vdots\\
				(X_{k-2},Y_{k-2}+ T^{(k-2)}(K_{k-3}))\\
				(X_{k-1},Z_{k-1}+ T^{(k-1)}(K_{k-2}))\\
				\vdots\\
				(X_n,Z_n+ T^{(n)}(K_{n-1}))\\
				\Lambda^{(k-1)}
			\end{array}	
			\right)	\;,
		\end{equation}
		we have
		\begin{align}
			& \left(K_0,\textsf{view}^{(k-1)}\right) \approx_{(2\varepsilon,\hat{s})} \left(K_0,\textsf{view}^{(k)}\right)	\;,\\
			& \widetilde{\textsf{H}}_{\infty}( K_{k-2} | E_{k-1}, \Lambda^{(k-1)}) \ge
			|K_{k-2}| - 2\ell \;.
		\end{align}
\end{claim}


\begin{proof}[Proof of Claim \ref{claim:induction_for_main}]
		To show this, we notice that in the previous view $\textsf{view}^{(k)}$, $E_k$ is independent of $X_{k-1}$ and only reveals $2\ell$ bits of information of $K_{k-1}$, which means that the key $K_{k-1}$ still has high (information theoretic) entropy given $E_k$. Since $Y_{k-1} = \textsf{F}_{K_{k-1}}(X_{k-1})$ and the function $\textsf{F}$ is $(\varepsilon,s)$-indistinguishable from uniform for any $2\ell$-weak key distribution, we get
		\begin{equation*}
			(X_{k-1},Y_{k-1},E_k )
			\approx_{(\varepsilon,s)}
			(X_{k-1},U_{|Y_{k-1}|},E_k )	\;,
		\end{equation*}
		which implies by the definition of pseudoentropy: 
		\begin{equation}
			\label{eq:induction_pseudoentropy}
			\textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(Y_{k-1}|\; X_{k-1}, E_k) = |Y_{k-1}|	\;.
		\end{equation}
		Then, we can bound the pseudoentropy of $Y_{k-1}$ in $\textsf{view}^{(k)}$ as
		\begin{equation*}
			\begin{aligned}
				 & \textsf{H}^{\textsf{rlx-HILL}}_{2\varepsilon,\hat{s}}\left(Y_{k-1}|\; X_{k-1}, R_{k-2} , E_k, \Lambda^{(k)} \right) \\
				 \overset{\text{(a)}}{\ge}	\  &  \textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(Y_{k-1}|\; X_{k-1}, R_{k-2}, E_k) - \ell 	\\
				 \overset{\text{(b)}}{=}	\  &  \textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(Y_{k-1}|\; X_{k-1},E_k) - \ell	\\
				 \overset{\text{(c)}}{=}	\  &  |Y_{k-1}| - \ell	\;,
			\end{aligned}
		\end{equation*}
		where $\hat{s} = \Theta(s\cdot \frac{\varepsilon^2}{2^{3\ell}\log^2 (1/\varepsilon)})$.
		The inequality (a) holds by Lemma \ref{lem:chain_rule_var_HILL}.
		The equality (b) holds since $R_{k-2}$ is independent of $(Y_{k-1}, X_{k-1},E_k)$.
		The equality (c) holds by \eqref{eq:induction_pseudoentropy}.
	
	
By the definition of HILL entropy and the fact that $R_{k-2}$ is independent of $(Y_{k-1}, X_{k-1},E_k)$, there exists a random variable $Z_{k-1}$ that
\begin{equation*}
	\left\{
	\begin{aligned}
	& \left(Z_{k-1},X_{k-1},E_k\right) \approx_{(\varepsilon,s)} \left(Y_{k-1},X_{k-1},E_k\right) 	\\
	& \widetilde{\textsf{H}}_{\infty}\left(Z_{k-1}|\; X_{k-1}, E_k, \right) \ge |Y_{k-1}| - \ell		\\
	& Z_{k-1}\; \text{is independent of} \; R_{k-2}
	\end{aligned}
	\right. \;,
\end{equation*}
which implies
\begin{equation*}
	\left\{
	\begin{aligned}
	& \left(Z_{k-1},X_{k-1},E_k, R_{k-2}\right) \approx_{(\varepsilon,s)} \left(Y_{k-1},X_{k-1},E_k, R_{k-2}\right) 	\\
	& \widetilde{\textsf{H}}_{\infty}\left(Z_{k-1}|\; X_{k-1}, R_{k-2}, E_k \right) \ge |Y_{k-1}| - \ell	\;.
	\end{aligned}
	\right. \;,
\end{equation*}

	By Lemma~\ref{lem:chain_rule_var_HILL}, there exists $\Lambda^{(k-1)}$, such that \todo{Isnt there a bug below since you have twice the same $\Lambda^{(k-1)}$?}
\begin{align}
	& \left(R_{k-2},Z_{k-1},X_{k-1},E_k,\Lambda^{(k-1)}\right) \approx_{(2\varepsilon,\hat{s})} \left(R_{k-2},Y_{k-1},X_{k-1},E_k,\Lambda^{(k-1)}\right) \label{eq:induction_ind_Zn_1}	\;,	\\
	& \widetilde{\textsf{H}}_{\infty}\left(Z_{k-1}|\; X_{k-1}, R_{k-2} , E_k, \Lambda^{(k)}\right) \ge |Y_{k-1}| - \ell	\;. \label{eq:induction_ind_Zn_2}
\end{align}
		
\todo{I don't understand how you get to (16) above?}	We replace $Y_{k-1}$ and $\Lambda^{(k)}$ in $\textsf{view}^{(k)}$ by $Z_{k-1}$ and $\Lambda^{(k-1)}$, giving rise to a new view $\textsf{view}^{(k-1)}$ with form \eqref{eq:induction_new_view}.
	Since $(K_0,\textsf{view}^{(k-1)})$ can be sampled efficiently from  $(R_{k-2},Z_{k-1},X_{k-1},E_k,\Lambda^{(k-1)})$, and $(K_0,\textsf{view}^{(k)})$ can be sampled efficiently from  $(R_{k-2},Y_{k-1},X_{k-1},E_k,\Lambda^{(k-1)})$ from \eqref{eq:induction_ind_Zn_1} we get
		\begin{equation}
			\label{eq:induction_result1}
			\left(K_0,\textsf{view}^{(k-1)}\right) \approx_{(2\varepsilon,\hat{s})} \left(K_0,\textsf{view}^{(k)}\right)	\;.
		\end{equation}
		From \eqref{eq:induction_ind_Zn_2}, we can show that $K_{k-2}$ has large residual entropy given the encryptions $E_{k-1} = (X_{k-1},Z_{k-1}+ T^{(k-1)}(K_{k-2}), E_{k})$:
		\begin{align}
		\label{eq:induction_result2}
			& \widetilde{\textsf{H}}_{\infty}( K_{k-2} | E_{k-1}, \Lambda^{(k-1)})	\nonumber\\	
			 \overset{\text{(a)}}{\ge} \ & \widetilde{\textsf{H}}_{\infty}( K_{k-2} | (X_{k-1},Z_{k-1}+ T^{(k-1)}(K_{k-2})), E_{k}) - \ell \nonumber\\
			 \overset{\text{(b)}}{\ge} \  & \textsf{H}_{\infty}(K_{k-2}) - (|Z_{k-1}| - \widetilde{\textsf{H}}_{\infty}(Z_{k-1}|X_{k-1},E_k)) - \ell \nonumber\\
			 \overset{\text{(c)}}{\ge} \  & \textsf{H}_{\infty}(K_{k-2}) - \ell - \ell \nonumber\\
			  = \ & |K_{k-2}| - 2\ell \;,
		\end{align}
		where we define that in the case $k=2$, $R_{-1} = \emptyset$.
		The inequality (a) holds by the chain rule of average min-entropy.
		The inequality (b) holds by Lemma \ref{lem:z_prg} and the fact $K_{k-2}$ is independent of $(Z_{k-1}, X_{k-1}, E_k)$.
		The inequality (c) holds by \eqref{eq:induction_ind_Zn_2}.
		By putting \eqref{eq:induction_result1} and \eqref{eq:induction_result2} together, this concludes the proof of the claim.
\end{proof}





		
		At the end of induction we get $\textsf{view}' = \textsf{view}^{(1)}$. We have that $(K_0,\textsf{view}') \approx_{(2n\varepsilon,\hat{s})} (K_0,\textsf{view}) $, by the triangle inequality for computational indistinguishability and the fact that $(K_0,\textsf{view}^{(i)}) \approx_{(2\varepsilon,\hat{s})} (K_0,\textsf{view}^{(i+1)})$, for $i\in\{1,\ldots,n\}$.
		We finally measure the average min-entropy of $K_0$ conditioned also on $W_0$ and use a similar argument as in~\eqref{eq:induction_result2}:
		\begin{align}
			& \widetilde{\textsf{H}}_{\infty}( K_{0} | \textsf{view'})	\nonumber\\	
			 =\ &  \widetilde{\textsf{H}}_{\infty}(K_0 | W_0, E_1, \Lambda^{(0)}) \nonumber \\
			 \ge \ & \widetilde{\textsf{H}}_{\infty}( K_0 | W_0, (X_1,Z_1+ T^{(1)}(K_{0})), E_2) - \ell \nonumber\\
			 \overset{\text{(a)}}{\ge} \  & \textsf{H}_{\infty}(K_0|W_0) - (|Z_1| - \textsf{H}_{\infty}(Z_1|X_1,E_2)) - \ell \nonumber\\
			 \ge \  & \textsf{H}_{\infty}(K_0|W_0) - \ell - \ell \nonumber\\
			  = \ & \textsf{H}_{\infty}(K_0|W_0) - 2\ell \;,
		\end{align}
		The inequality (a) holds by Lemma \ref{lem:z_prg} as well as the fact $K_0$ is independent of $(Z_1,X_1,E_2)$, and $Z_1$ is independent of $W_0$. \todo{Don't you use in the above also somewhere that $\textsf{H}_{\infty}(Z_1|X_1,E_2))$ has high min-entropy?}
 	This completes the proof.
	\end{proof}

	
	
	
	

