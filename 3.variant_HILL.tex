
\section{A New Relaxed HILL Entropy Notion for Leakage Resilient Cryptography}
In the case of leakage resilient cryptography, relaxed HILL entropy does not help us much, since with relaxed HILL, we have to replace not only $X$, but also the whole conditioning part ($Y$ and $A$). We need a new notion of computational entropy, such that:
\begin{itemize}
	\item the chain rule holds,
	\item the conditioning part needs minimum replacement for switching to information theoretical settings.
\end{itemize}
	
	We define a new relaxed version of conditional HILL entropy for leakage resilient cryptography, which is able to keep the conditioning part unchanged except the bounded leakage, while satisfying the chain rule.
		
\begin{definition}[A new relaxed version of HILL entropy in the presence of leakage]
	\label{def:var_HILL}
	$X$ has leak-HILL entropy at least $k$ conditioning on $Y$ and the leakage $A$, denoted $\textsf{H}^{\textsf{leak-HILL}}_{\varepsilon,s}(X|Y,A)\ge k$, if there exist random variables $Z$ and $A'$, such that $H_{\infty}(Z|Y,A')\ge k$ and no distinguisher of size $s$ can distinguish $(X,Y,A)$ and $(Z,Y,A')$ with advantage more than $\varepsilon$.
\end{definition}
	
	This is also a relaxed version of conditional HILL entropy, but stronger than the relaxed-HILL entropy defined in Definition~\ref{def:relaxed_HILL}. In this notion we cannot replace the conditioning part except the leakage. 
	
This leak-HILL entropy satisfies the conditional chain rule. 
\begin{lem}[Chain Rule for the Leak-HILL Entropy]
	\label{lem:chain_rule_var_HILL}
	For any random variables $X$ and $Y$ and the leakage $A$ of length bounded by $\ell$ bits, it holds that 
	\begin{equation*}
	\textsf{H}^{\textsf{leak-HILL}}_{2\varepsilon,\hat{s}}(X|Y,A) \ge \textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(X|Y) - \ell	\;,
	\end{equation*}
	where $\hat{s} = \Theta(s\cdot \frac{\varepsilon^2}{2^{3\ell}\log^2 (1/\varepsilon)})$.
	
	Moreover, for any $Z$, such that
	\begin{equation*}
	\left\{
	\begin{aligned}
		& (Z,Y) \approx_{(\varepsilon,s)} (X,Y) \\
		& \textsf{H}_\infty(Z|Y) \ge \textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(X|Y) 
	\end{aligned}
	\right. \;,
	\end{equation*}
	there exists an $A'$, such that
	\begin{equation*}
	\left\{
	\begin{aligned}
		& (Z,Y,A') \approx_{(2\varepsilon,\hat{s})} (X,Y,A) \\
		& \textsf{H}_\infty(Z|Y,A') \ge  \textsf{H}^{\textsf{leak-HILL}}_{2\varepsilon,\hat{s}}(X|Y,A)
	\end{aligned}
	\right. \;.
	\end{equation*}
\end{lem}

\begin{proof}
	It is equivalent to prove that $\textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(X|Y) \ge k$ implies $\textsf{H}^{\textsf{leak-HILL}}_{2\varepsilon,\hat{s}}(X|Y,A) \ge k - \ell$. 
	We can prove this using simulator-based construction, similar to the proof of Proposition 2 in \cite{JetchevP14}.
	Recall that $\textsf{H}^{\textsf{HILL}}_{\varepsilon,s}(X|Y) \ge k$ means that there exists a random variable $Z$ such that $\textsf{H}_\infty(Z|Y) \ge k$ and $(X,Y) \approx_{(\varepsilon,s)} (Z,Y)$. For any such $Z$, and any $(\hat{\varepsilon},\hat{s})$, by Theorem 1 in \cite{JetchevP14}, there exists a simulator $h$ of size $s_h=O(\hat{s}\cdot \frac{2^{3\ell}\log^2 (1/\hat{\varepsilon})}{\hat{\varepsilon}^2})$ such that 
			\begin{equation*}
				(X,Y,A) \approx_{(\hat{\varepsilon},\hat{s})} (X,Y,h(X,Y)) \approx_{(\varepsilon,s-s_h)} (Z,Y,h(Z,Y))	\;.
			\end{equation*}
			The second step follows from $(X,Y) \approx_{(\varepsilon,s)} (Z,Y)$ and the fact that $h$ has complexity $s_h$. Using the triangle inequality for computational indistinguishability, we get 
			\begin{equation*}
				(X,Y,A) \approx_{(\hat{\varepsilon}+\varepsilon,\min(\hat{s},s-s_h))} (Z,Y,h(Z,Y))	\;.
			\end{equation*}
			By setting $\hat{\varepsilon}=\varepsilon$ and $\hat{s} = \Theta(s\varepsilon^2/2^{3\ell}\log^2(1/\varepsilon))$ and choosing proper hidden constant coefficient in the $\Theta$ to achieve $s_h = O(s)$ and $s_h\le s/2$, we get 
			\begin{equation}
				\label{chainrule_proof_1}
				(X,Y,A)\approx_{(2\varepsilon,\hat{s})} (Z,Y,h(Z,Y))	\;.
			\end{equation}
			Using the chain rule for average min-entropy, we get
			\begin{equation}
				\label{chainrule_proof_2}
				\widetilde{\textsf{H}}_\infty(Z|Y,h(Z,Y)) \ge \widetilde{\textsf{H}}_\infty(Z|Y) - |h(Z,Y)| \ge k - \ell.
			\end{equation}
			From \eqref{chainrule_proof_1} and \eqref{chainrule_proof_2}, we get $\textsf{H}^{\textsf{leak-HILL}}_{2\varepsilon,\hat{s}}(X|Y,h(X,Y)) \ge k - \ell$.
			
The second statement of the Lemma follows directly from the proof structure.
		\end{proof}